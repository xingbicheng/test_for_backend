# 后台管理系统
仅供后端人员测试使用  
如果您对DataTables，jquery，layui 有所了解的话，阅读README.md足够  
如果不了解的话，请阅读  
详细使用说明地址: https://segmentfault.com/a/1190000017765398
## 项目依赖
- jQuery
- DataTables
- layUI
## 使用注意
- 请勿随意改动或删减`/lib`,`/public`,`/static`目录文件内容
- 本项目不需要安装任何类库框架
- 运行`/src/index.html`即可运行项目
- 只能从`/src/index.html`进入，其他文件打开无效
## 使用流程
- 后台传入参数，自动生成
- 在`/src/pages/`目录下创建相应页面文件夹
- 进行拷贝
- 修改`/src/index.html`,创建与新页面的链接
- 对新页面稍作修改
- 修改`/src/config/config.js`文件，配置相应`userData`和`API`
- 运行项目
## 项目目录
```$xslt
\根目录
├─lib                   第三方插件
├─public                    公共部分
│  ├─font                   字体
│  ├─html                   公共html模板
│  └─img                    公共图片
├─src                   代码
│  ├─config                 配置文件
│  ├─pages                  页面文件
│  │   ├─desktop                    首页/我的桌面
│  │   └─meeting                    会议/示例页面                   
│  ├─utils                  公共数据及函数
│  └─index.html                 主界面/入口
├─README.md                   使用方法
└─static                    静态资源
```
## 项目图示
![image](https://gitlab.com/xingbicheng/test_for_backend/raw/master/public/img/desktop.png)

![image](https://gitlab.com/xingbicheng/test_for_backend/raw/master/public/img/meeting.png)

![image](https://gitlab.com/xingbicheng/test_for_backend/raw/master/public/img/meeting-add.png)

![image](https://gitlab.com/xingbicheng/test_for_backend/raw/master/public/img/meeting-edit.png)

![image](https://gitlab.com/xingbicheng/test_for_backend/raw/master/public/img/meeting-delete.png)